#!/bin/sh

echo "UI_KEY=$UI_KEY"

export PARAM=$(test $DFX_NETWORK && echo $DFX_NETWORK || echo "local")

dfx canister --network $PARAM call etboom_backend generateUUID "(\"$UI_KEY\")" | grep 'ok'

exit $?
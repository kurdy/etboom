#!/bin/sh
# Update canister data
# This script uses the timezone data source described here https://www.geonames.org/export/web-services.html#timezone
#
# It downloads the content to a temporary file, then formats the data to pass as parameter to the replaceAll function
# 
# The default url is https://download.geonames.org/export/dump/timeZones.txt. It can be passed as parameter.


export PARAM=$(test $DFX_NETWORK && echo $DFX_NETWORK || echo "local")

SOURCE_URL=$1
if [ -z "$SOURCE_URL" ]; then
    SOURCE_URL="https://download.geonames.org/export/dump/timeZones.txt"
fi

echo "Use source url: $SOURCE_URL"

STATUS=$((0)) 

# TEST_LINE='CI	Africa/Abidjan	0.0	0.0	0.0'
# echo $TEST_LINE | awk -F" " '{print "record {timezoneId=\""$2"\";countryCode=\""$1"\";dstOffset="$4";rawOffset="$5";gmtOffset="$3"};"}'

TMP_FILE=$(mktemp)

`wget $SOURCE_URL -O $TMP_FILE`

DATA="(vec{"

while IFS= read -r line
do
    ITEM=$(awk -F" " '{print "record {timezoneId=\""$2"\";countryCode=\""$1"\";dstOffset="$4";rawOffset="$5";gmtOffset="$3"};"}')
    DATA="$DATA$ITEM"
done <<< $(tail -n +2 $TMP_FILE)

DATA="$DATA})";

dfx canister --network $PARAM call tzService replaceAll "$DATA" | grep ok

STATUS=$?

rm $TMP_FILE

echo "Number of timezones: $(dfx canister --network $PARAM call tzService getNumberOfTimezone)"

exit $STATUS
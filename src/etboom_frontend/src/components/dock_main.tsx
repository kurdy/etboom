import React from 'react';
import { useNavigate } from 'react-router-dom';

import { Dock } from 'primereact/dock';
import { Button } from 'primereact/button';
import { confirmDialog } from 'primereact/confirmdialog';
import {useIntl} from "react-intl";



const DockMain = () => {

  const navigate = useNavigate();

  const intl = useIntl();

  const accept = async () => {
    navigate('/logout');
  }
  
  const reject = async () => {
    console.info("Close dialog. Do nothing");
  }
  
  const logOutDialog = async () => {
    confirmDialog({
        message: intl.formatMessage({ id: 'key.dock.main.dialog.message',defaultMessage: 'Logout Confirmation'}),
        header: intl.formatMessage({ id: 'key.dock.main.dialog.header',defaultMessage: 'Logout'}),
        icon: 'pi pi-sign-out',
        accept: accept,
        reject: reject
    });
  };

  const items = [
      {
          label: intl.formatMessage({ id: 'key.dock.main.profile.label',defaultMessage: 'Profile'}),
          icon: ()=><Button icon="pi pi-user" className="p-button-rounded p-button-secondary" aria-label={intl.formatMessage({ id: 'key.dock.main.profile.label',defaultMessage: 'Profile'})} onClick={()=>{alert('Profile')}}/>
      },
      {
        label: intl.formatMessage({ id: 'key.dock.main.settings.label',defaultMessage: 'Settings'}),
        icon: ()=><Button icon="pi pi-cog" className="p-button-rounded p-button-secondary" aria-label={intl.formatMessage({ id: 'key.dock.main.settings.label',defaultMessage: 'Settings'})} onClick={()=>{alert('Settings')}}/>
      },
      {
        label: intl.formatMessage({ id: 'key.dock.main.logout.label',defaultMessage: 'Logout'}),
        icon: ()=><Button icon="pi pi-sign-out" className="p-button-rounded p-button-secondary" aria-label={intl.formatMessage({ id: 'key.dock.main.logout.label',defaultMessage: 'Logout'})} onClick={logOutDialog}/>
      }
  ];

  return (
      <Dock model={items} position="right"/>
  )
}

export default DockMain;
import React,{ Fragment,useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import {logout,localLogout} from '../services/auth';

interface LogoutProps {
  onChange: (isLogged : boolean) => void
}

const Logout = (Props: LogoutProps) => {
  
  const navigate = useNavigate();

  const doLogout = async () => {
    await logout();
    await localLogout();
    Props.onChange(false);
    navigate('/');

  }

  useEffect(() => {
    doLogout();
  }),[];
  

  return (
    <Fragment/>
  );
}

export default Logout;
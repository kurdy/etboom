// DFINITY Brand Material https://dfinity.frontify.com/d/XzkdhhDptijE/dfinity-brand-guide#/internet-computer/logo
// To component https://react-svgr.com/playground/
import * as React from "react"

interface LogoProps {
  size?: string,
}

const DfinityLogo = (props: LogoProps) => {

  const value : string = props.size ? props.size : '230.391';

  const n = value.match(/[+-]?\d+(?:\.\d+)?/g);
  const u = value.toLowerCase().trim().match(/[a-z]+/g)

  const size : number = parseFloat(n ? n[0] : '230.391');
  const unit : string = u ? u[0] : '';
  
  const width = `${size}${unit}`;
  // 2.117642193 = 230.391 / 108.796 (orignal size) 
  const height = `${size / 2.117642193}${unit}`; // 2.117642193 = 230.391 / 108.796

  console.debug(`${width} ${height}`);
  
  return (
    <svg
      role="img"
      aria-labelledby="aria-label"
      viewBox="0 0 60.958 28.786"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      xmlns="http://www.w3.org/2000/svg"
      style={{
        height: height,  
        width: width,
      }}
      {...props}
    >
      <title id="aria-label">Logo DFINITY</title>
      <defs>
        <linearGradient
          xlinkHref="#a"
          id="c"
          gradientUnits="userSpaceOnUse"
          x1={145.304}
          y1={7.174}
          x2={221.385}
          y2={85.958}
          gradientTransform="translate(-399.202 -213.945)"
        />
        <linearGradient
          id="a"
          x1={145.304}
          y1={7.174}
          x2={221.385}
          y2={85.958}
          gradientUnits="userSpaceOnUse"
        >
          <stop offset={0.21} stopColor="#F15A24" />
          <stop offset={0.684} stopColor="#FBB03B" />
        </linearGradient>
        <linearGradient
          xlinkHref="#b"
          id="d"
          gradientUnits="userSpaceOnUse"
          x1={85.087}
          y1={101.622}
          x2={9.006}
          y2={22.838}
          gradientTransform="translate(-399.202 -213.945)"
        />
        <linearGradient
          id="b"
          x1={85.087}
          y1={101.622}
          x2={9.006}
          y2={22.838}
          gradientUnits="userSpaceOnUse"
        >
          <stop offset={0.21} stopColor="#ED1E79" />
          <stop offset={0.893} stopColor="#522785" />
        </linearGradient>
      </defs>
      <path
        d="M-224.77-213.945c-12.878 0-26.918 6.6-41.757 19.6-7.04 6.159-13.12 12.759-17.68 18.038l.04.04v-.04s7.2 7.84 15.16 16.24c4.28-5.08 10.44-12 17.519-18.24 13.2-11.559 21.799-13.999 26.719-13.999 18.52 0 33.559 14.68 33.559 32.719 0 17.92-15.08 32.599-33.56 32.719-.84 0-1.92-.12-3.28-.4 5.4 2.32 11.2 4 16.72 4 33.919 0 40.559-22.12 40.999-23.72 1-4.04 1.52-8.28 1.52-12.64 0-29.918-25.12-54.317-55.958-54.317z"
        fill="url(#a)"
        style={{
          fill: "url(#c)",
        }}
        transform="matrix(.26458 0 0 .26458 105.622 56.606)"
      />
      <path
        d="M-343.245-105.149c12.88 0 26.92-6.6 41.759-19.6 7.04-6.16 13.12-12.759 17.679-18.039l-.04-.04v.04s-7.2-7.84-15.16-16.24c-4.28 5.08-10.439 12-17.519 18.24-13.2 11.56-21.799 14-26.719 14-18.519-.04-33.558-14.72-33.558-32.76 0-17.918 15.079-32.598 33.558-32.718.84 0 1.92.12 3.28.4-5.4-2.32-11.2-4-16.72-4-33.918 0-40.518 22.12-40.998 23.68-1 4.08-1.52 8.28-1.52 12.639 0 29.999 25.12 54.398 55.958 54.398z"
        fill="url(#b)"
        style={{
          fill: "url(#d)",
        }}
        transform="matrix(.26458 0 0 .26458 105.622 56.606)"
      />
      <path
        d="M49.687 23.864c-4.593-.116-9.366-3.735-10.34-4.635-2.518-2.328-8.329-8.625-8.783-9.122C26.309 5.334 20.54 0 14.806 0H14.785C7.82.032 1.967 4.752.401 11.049c.117-.413 2.413-6.382 10.837-6.17 4.593.116 9.387 3.788 10.371 4.688 2.52 2.328 8.33 8.625 8.784 9.122 4.255 4.763 10.022 10.097 15.758 10.097H46.173c6.964-.032 12.827-4.752 14.382-11.049-.127.413-2.434 6.329-10.868 6.127z"
        fill="#29abe2"
      />
    </svg>
  )
}

export default DfinityLogo;


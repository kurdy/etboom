import React from 'react';

import DockMain from '../components/dock_main';

import { ConfirmDialog} from 'primereact/confirmdialog';

const Home = () => {

  return (
    <div>
      <ConfirmDialog/>
      <DockMain />
    </div>
  )
}

export default Home;
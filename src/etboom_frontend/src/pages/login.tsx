import React,{useState, Fragment, useEffect} from 'react';
import {login,whoami,localLogin} from '../services/auth'
import {FormattedMessage,useIntl} from "react-intl";
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { RadioButton } from 'primereact/radiobutton';
import  DfinityLogo from '../components/dfinity_logo';
import TotemHead from '../components/totem_head';

enum Identity {
  InternetIdentity,
  LocalIdentity,
  NFID,
}

interface LoginProps {
  onChange: (isLogged : boolean) => void
}

const Login = (props:LoginProps) => {

  const intl = useIntl();
  const [identity, setIdentity] = useState(Identity.InternetIdentity);
  const [isLoginInProgress,setLoginInProgress] = useState(false); // The login process is in progress

  useEffect(() => {
    if(isLoginInProgress) {
      switch (identity) {
        case Identity.InternetIdentity : {
          login().then(()=>{
            props.onChange(true);
            setLoginInProgress(false);
            whoami().then((id)=>{
              console.info(`Internet Identity Principal: ${id.toString()}`);
            });
            window.location.reload();
          });
          break;
        }
        case Identity.LocalIdentity : {
          localLogin().then(()=>{
            props.onChange(true);
            setLoginInProgress(false);
            //window.location.reload();
          });
          break;
        }
        case Identity.NFID : {
          setLoginInProgress(false);
          break;
        }
        default : {
          setLoginInProgress(false);
          break;
        }
      }
    }
  }),[isLoginInProgress];
  
  const handleBtnConnect = () => {
    setLoginInProgress(true);
  }

  const cardStyle = {
    margin: '10px',
    minWidth: '30vw',
    maxWidth: '80vw',
  }

  const title = (
    <h1 style={{textAlign: 'center'}}>
      <FormattedMessage defaultMessage='Login' id='key.login.title'/>
    </h1>
  )

  const subTitle = (
    <div>
       <div className="field-radiobutton">
          <DfinityLogo size={'1.5vw'}/>
          <RadioButton style={{marginLeft: '15px'}} inputId="internetIdentity" name="identity" value={Identity.InternetIdentity} onChange={(e) => setIdentity(e.value)} checked={identity === Identity.InternetIdentity} />
          <label htmlFor="internetIdentity"><FormattedMessage defaultMessage='Using Internet Identity' id='key.login.label.internet.identity'/></label>  
        </div>
        <div className="field-radiobutton">
          <TotemHead size={'1.5vw'} background={'var(--surface-ground)'} color={'var(--primary-color)'}/>  
          <RadioButton style={{marginLeft: '15px'}} inputId="localIdentity" name="identity" value={Identity.LocalIdentity} onChange={(e) => setIdentity(e.value)} checked={identity === Identity.LocalIdentity} />
          <label htmlFor="localIdentity"><FormattedMessage defaultMessage='Using Local Identity' id='key.login.label.local.identity'/></label>  
        </div>
    </div>
  )

  const footer = () => {
    switch (identity) {
      case Identity.InternetIdentity : {
        return(
          <Fragment>
            <p><FormattedMessage  id='key.login.ii.info'
                defaultMessage="It has long been known that usernames and passwords have been a weak point of an individual's security on the web. In order to combat this and provide increased security to users on the Internet Computer (IC), the Internet Identity (II) blockchain authentication system was developed. Internet Identity enables you to use your devices to authenticate and sign in pseudonymously to dapps on the Internet Computer. Anyone with a traditional HSM device, such as a YubiKey, or mobile devices that contain a TPM chip, such as a laptop or phone can generate Internet Identities and start using dapps on the IC."
              />
            </p>
            <a href='https://medium.com/dfinity/internet-identity-the-end-of-usernames-and-passwords-ff45e4861bf7'
               hrefLang='en' rel="noreferrer" target={'_blank'}><FormattedMessage defaultMessage='Learn more (In English)' id='key.login.ii.learn'/></a>
          </Fragment>
        );
      }
      case Identity.LocalIdentity : {
        return(
          <Fragment>
            <p><FormattedMessage  id='key.login.li.info'
                defaultMessage="The local identity is an ID saved locally on this browser. It does not require an account. However, if the browser data is deleted, there is no way to retrieve it and the link to your data will be lost."
              />
            </p>
          </Fragment>
        );
      }
      case Identity.NFID : {
        return <Fragment/>
      }
      default : {
        return <Fragment/>
      }
    }
    
  }

  return(
    
    <div className='container-row'>
      <div className='item'>
        <Card title={title} subTitle={subTitle} footer={footer} className='card' style={cardStyle}>
          <div className='align-center'>
           <Button disabled={isLoginInProgress} style={{width: '100%',textTransform: 'uppercase'}} label={intl.formatMessage({ id: 'key.login.btn.login.label',defaultMessage: 'Login'})} className="p-button-rounded" onClick={()=>handleBtnConnect()}/>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default Login;
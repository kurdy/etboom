import React from 'react';
import {FormattedMessage} from "react-intl";

interface NotFoundProps {
  fgColor?: string,
  bgColor?: string,
}

const NotFound = (Props?: NotFoundProps) => {

  const fgStyle = {
    color: Props? Props.fgColor || 'var(--primary-color)' : 'var(--default-text-color)',
    fontSize: '8vw',
  };

  const bgStyle = {
    background: Props? Props.bgColor || 'var(--bg-primary)' : 'var(--default-background)',
  };
  
  return(
    
    <div className='container-row' style={bgStyle}>
      <div className='item' style={fgStyle}>
        <FormattedMessage defaultMessage='Route not found' id='key.component.notfound'/>
      </div>
    </div>
  )
}

export default NotFound;
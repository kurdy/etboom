import React from 'react';

import "../../assets/loader.css"; 

interface LoaderProps {
  dotColor?: string,
  bgColor?: string,
}

const Loader = (Props?: LoaderProps) => {

  const dotStyle = {
    background: Props? Props.dotColor || 'var(--primary-color)' : 'var(--default-text-color)',
  };

  const bgStyle = {
    background: Props? Props.bgColor || 'var(--bg-primary)' : 'var(--default-background)',
  };
  
  return(
    <div className='container-row' style={bgStyle}>
      <div className='lds-ellipsis item' style={bgStyle}><div style={dotStyle}></div><div style={dotStyle}></div><div style={dotStyle}></div><div style={dotStyle}></div></div>
    </div>
  )
}

export default Loader;
import React,{useEffect, useState,lazy, Suspense} from 'react';
import { HashRouter as Router, Route, Routes } from 'react-router-dom';
import { IntlProvider} from "react-intl";
import {defaultLocale,defaultMessages,getMessages,locales,getLocale,setLocale} from './services/i18n';
import {isAutenticated,isLocalLoginActive} from './services/auth';
import Loader from './pages/loader'
import Lang from './components/lang';
import Logout from './components/logout';

const Login = lazy(() => import('./pages/login'));
const Home = lazy(() => import('./pages/home'));
const NotFound = lazy(() => import('./pages/notFound'));

import "../assets/main.css";

/**
 * Start Prime section
 */
import PrimeReact from 'primereact/api';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

import 'primereact/resources/themes/lara-dark-teal/theme.css';
//import 'primereact/resources/themes/lara-light-teal/theme.css';
// Ripple is an optional animation for the supported components such as buttons.
PrimeReact.ripple = true;

// Input fields come in two styles, default is outlined with borders around the field whereas filled
PrimeReact.inputStyle = 'filled';
/**
 * End Prime section
 */


const App = () => {
  const [language,setLanguage] = useState(defaultLocale);
  const [messages,setMessages] = useState(defaultMessages);
  const [entry, setEntry] = useState(Loader);
  const [isLogged, setLogged] = useState(false);

  useEffect(() => {
    if (!isLogged) {
      isLocalLoginActive().then(b=>{
        setLogged(b);
        if(b){
          setEntry(<Home/>);
        } else {
          isAutenticated().then(b=>{
            setLogged(b);
            if(b){
              setEntry(<Home/>);
            } else {
              setEntry(<Login onChange={setLogged}/>);
            }
          });
        }
      }); 
    }
    
    const lang = getLocale();
    if (locales.some(i => i === lang)) {
      console.info(`set locale = ${lang}`)
      setLanguage(lang);
      getMessages(lang).then(m=>setMessages(m));
      setLocale(lang);
    }
  },[language,isLogged])

  return (
    <IntlProvider messages={messages} locale={language} defaultLocale={defaultLocale}>
      <Router>
        <Lang onLanguageChange={setLanguage}/>
        <Suspense fallback={<Loader />}>
          <Routes> 
            <Route path="/" element={entry}/>
            <Route path="/logout" element={<Logout onChange={setLogged}/>}/>
            <Route path="/loader" element={<Loader/>}/>
            <Route path="*" element={<NotFound fgColor='var(--red-600)'/>}/>
          </Routes>
        </Suspense>
      </Router>
    </IntlProvider> 
  );
}

export default App;
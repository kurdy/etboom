import en from "../../assets/locale/en.json"; //default
import {locale as primeLocale,addLocale as primeAddLocale,updateLocaleOption} from 'primereact/api';

const STORE_KEY = "app.locale";

export const defaultLocale = 'en';
export const locales = ['en','fr'];
export const defaultMessages : Record<string,string> = en;

export const getLocale = () : string => {return (localStorage.getItem(STORE_KEY) || navigator.language).substring(0,2).toLowerCase();};

export const setLocale = async (lang : string) => localStorage.setItem(STORE_KEY,lang);

export const removeLocale = async () => localStorage.removeItem(STORE_KEY);

export const getMessages = async (l : string) : Promise<Record<string,string>> => {
  const lang = l.toLocaleLowerCase();
  fetchPrimeLocale(lang);
  if (lang.toLowerCase() === defaultLocale) {
    return defaultMessages;
  } else {
    const response = await fetch('locale/'+ lang + '.json');
    if (response.ok) {
      return response.json();
    } else {
      console.error(`Could not get messages for lang=${lang}`);
      return defaultMessages; 
    }
  }
}

const fetchPrimeLocale = async (lang : string) => {
  if (lang != defaultLocale && locales.some(i => i === lang)) {
    fetch('locale/primelocale/'+ lang +'.json').then((r)=>{
        r.json().then((data)=>{
            console.debug(`Add prime locale for ${lang}`);
            primeAddLocale(lang,data[lang]);
            primeLocale(lang);    
            primeLocale(lang);
            updateLocaleOption('dateFormat',indentifyShortDateFormat(),lang);
        });
    });
  }
}

const indentifyShortDateFormat = () => {
  const options : Intl.DateTimeFormatOptions = { month: "numeric", day: "numeric", year: "numeric" };
  const date = Date.parse("1985-11-23T22:22:22.222Z");
  let formatedDate = new Intl.DateTimeFormat(navigator.language, options).format(date);
  formatedDate=formatedDate.replace("1985","yy");
  formatedDate=formatedDate.replace("11","mm");
  formatedDate=formatedDate.replace("23","dd");
  return formatedDate;

}
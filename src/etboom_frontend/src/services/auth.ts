import { createActor,canisterId  } from '../../../declarations/etboom_backend';
import { AuthClient } from "@dfinity/auth-client";
import {etboom_backend} from '../../../declarations/etboom_backend';
//import { _SERVICE } from "../../../declarations/etboom_backend/etboom_backend.did";

const ONE_DAY = BigInt(24 * 60 * 60 * 1000 * 1000 * 1000);

export const whoami = async () : Promise<string> => {
  const authClient = await AuthClient.create();
  const identity = await authClient.getIdentity();
  console.debug("identity: " + JSON.stringify(identity));
  const actor = createActor(canisterId, {
      agentOptions: {
      identity,
      },
  });
  console.debug("actor: " + JSON.stringify(actor));
  const id = await actor.whoami();
  return id.toString();
}

export const isAutenticated = async () : Promise<boolean> => {
  let isAutenticated = false;
  const authClient = await AuthClient.create();
  isAutenticated = await authClient.isAuthenticated();
  console.info(`isAutenticated using II ${isAutenticated}`)
  return isAutenticated;
}

export const logout = async () : Promise<void> => {
  const authClient = await AuthClient.create();
  authClient.logout();
}

export const login = async (callback? : () => void) => {
  let url = "https://identity.ic0.app"; 
  if(process.env.LOCAL_II_CANISTER) {
    url = process.env.LOCAL_II_CANISTER;
  }
  console.info(`url II : ${url}`);
  const authClient = await AuthClient.create();
  const fCb = callback ? callback : handleLogin;
  await authClient.login({
      maxTimeToLive: ONE_DAY,
      onSuccess: async () => {
        fCb();
      },
      identityProvider: url
    });

}

const handleLogin = () => {
  console.info("Sign in successful");
}

/***
 * Section local login
 */


const LOCAL_LOGIN_STORE_KEY = "local.login";

interface LocalLogin {
  isActive: boolean,
  id: Uint8Array | number[],
}

export const localLogout = async () : Promise<void>  => {
  const json = localStorage.getItem(LOCAL_LOGIN_STORE_KEY);
  if(json){
    const data = JSON.parse(json);
    localStorage.setItem(LOCAL_LOGIN_STORE_KEY,JSON.stringify({
      isActive : false,
      id : data.id,
    }));
  }
}

export const isLocalLoginActive = async () : Promise<boolean>  => {
  const json = localStorage.getItem(LOCAL_LOGIN_STORE_KEY);
  if(json){
    const data = JSON.parse(json);
    return data.isActive;
  }
  return false;
}

export const localLogin = async () => {
  const json = localStorage.getItem(LOCAL_LOGIN_STORE_KEY);
  if(json){
    console.log("json found");
    const data = JSON.parse(json);
    
    const isValid = await etboom_backend.isValidUUID(new Uint8Array(new ArrayBuffer(data.id)));
    if (!isValid) {
      console.warn(`ID ${ new Uint8Array({...data.id})} is invalid.`);
      localStorage.removeItem(LOCAL_LOGIN_STORE_KEY);
      localStorage.setItem(`${LOCAL_LOGIN_STORE_KEY}.invalid`,JSON.stringify({
        isActive : false,
        id : data.id,
      }));
    } else {
      localStorage.setItem(LOCAL_LOGIN_STORE_KEY,JSON.stringify({
        isActive : true,
        id : data.id,
      }));
    }
  } else {
    console.info("No JSON -> createLocalLogin");
    await createLocalLogin();
  }
} 

const createLocalLogin = async () => {
  const uiKey = process.env.UI_KEY || '';
  try {
    const response = await etboom_backend.generateUUID(uiKey);
    if('ok' in response) {
      const data : LocalLogin = {
        isActive : true,
        id : response.ok,
      }
      localStorage.setItem(LOCAL_LOGIN_STORE_KEY,JSON.stringify(data));
      console.info(`Data from generateUUID ${localStorage.getItem(LOCAL_LOGIN_STORE_KEY)}`);
    } else if ('err' in response) {
      console.error(`Error from generateUUID: ${response.err}`);
    } else {
      console.error('Unexpected object from generateUUID');
    }
  } catch (error) {
    console.warn(`Error during call of generateUUID: ${error}`);
  }

}
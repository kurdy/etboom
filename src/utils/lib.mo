/// utils module implementation
/// Gathers some useful and shareable functions and constants  
import Int "mo:base/Int";
import Hash "mo:base/Hash";
import XXH32 "../xxh32";

module {

  /// Represents 1 day in nano seconds
  public let ONE_DAY : Int = 86_400_000_000_000; //ns

  public func makeIntHash (value : Int) : Hash.Hash {
    return XXH32.textHash(Int.toText(value));
  };
}
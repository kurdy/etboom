import Result "mo:base/Result";
import Principal "mo:base/Principal";
import Debug "mo:base/Debug";
import SHA2 "../sha2";
import Blob "mo:base/Blob";
import Nat8 "mo:base/Nat8";
import Nat "mo:base/Nat";
import Int "mo:base/Int";
import Array "mo:base/Array";
import Iter "mo:base/Iter";
import Random "mo:base/Random";
import Option "mo:base/Option";
import Text "mo:base/Text";
import HashMap "mo:base/HashMap";
import Time "mo:base/Time";
import Utils "../utils";


shared({caller = canisterOwner}) actor class EtBoomBackEnd() = this {

  type Result<T,E> = Result.Result<T,E>;


  // Data store used during upgrade
  private stable var uuidStore : [(Int, Blob)] = [];
  private var uuidSize : Nat = 10;

  // Current UUID
  private var uuidMap = HashMap.fromIter<Int, Blob>(
      uuidStore.vals(),uuidSize,func (x, y) { Int.equal(x,y)}, func(x){Utils.makeIntHash(x)});

  system func preupgrade() {
    Debug.print ("Call of EtBoomBackEnd:preupgrade.");
    uuidStore := Iter.toArray(uuidMap.entries());
    uuidSize := Iter.size<(Int, Blob)>(uuidStore.vals());  
  };

  system func postupgrade() {
    Debug.print ("Call of EtBoomBackEnd:postupgrade.");
    uuidStore:=[];
    uuidSize:=10;
  };

  private stable var uiKey : Text = "";

  public shared (msg) func setUiKey(key : Text) : async Result<(),Text>{
    Debug.print(Principal.toText(msg.caller));
    if(Principal.equal(canisterOwner,msg.caller)) {
      uiKey := key;
      #ok();
    } else {
      #err("Unauthorised operation");
    }   
  };

  public func generateUUID(uiKey : Text) : async Result<Blob,Text> {
    if(not isUiKeyValid(uiKey)) {
      return #err("Invalid key.");
    };
    var rand = Random.Finite(await Random.blob());
    let data : [var Nat8] = Array.init<Nat8>(64,0);
    for (i in Iter.range(0, 63)) {
      if ((i % 32) == 0 and i > 0) {
        rand := Random.Finite(await Random.blob());
      };
      let value = rand.byte();
      switch value {
        case (?v) data[i] := v;
        case null return #err("Could not get value");  
      };
    };
    let id = SHA2.fromIter(#sha256,data.vals());
    uuidMap.put(Time.now(),id);
    #ok(id);    
  };

  public func isValidUUID(key : Blob) : async Bool {
    return isUuidValid(key);
  };

  public shared (msg) func whoami() : async Principal {
    msg.caller
  };


  /***
   * Private area
   ***/

  private func isUiKeyValid (key : Text) : Bool {
    Text.size(uiKey) == 128 and Text.equal(uiKey,key);
  };

  private func isUuidValid (id : Blob) : Bool {
    let size = Iter.size(Array.vals(Blob.toArray(id)));
    //size == 32 and isUuidExist(id);
    Debug.print("Size=" # Nat.toText(size));
    true;
  };

  private func isUuidExist (id : Blob) : Bool {
    let uuids = uuidMap.vals(); 
    let fuuids = Iter.filter(uuids,func (x : Blob): Bool{Blob.equal(x,id)});
    switch (Iter.size(fuuids)) {
      case 0 return false;
      case 1 return true;
      case _ Debug.trap("UUID maps contains duplicate UUID. 😱");
    }
  };

};
